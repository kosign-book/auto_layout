//
//  SupportMultiDeviceVC.swift
//  AutoLayoutIOS
//
//  Created by kimlang on 2/15/23.
//

import UIKit

class SupportMultiDeviceVC: UIViewController {

    
        // Mark: - IBOutlet
        @IBOutlet weak var viewPF: UIView!
        @IBOutlet weak var buttonView                   : UIView!
        
        @IBOutlet weak var heightButtonContraint        : NSLayoutConstraint!
        @IBOutlet weak var heightStackViewConstraint    : NSLayoutConstraint!
        
        // Mark: - Lifecycle
        override func viewDidLoad() {
            super.viewDidLoad()
            
            //set corner Raduis for button View
            buttonView.layer.cornerRadius   = 15
            buttonView.layer.maskedCorners  = [.layerMinXMaxYCorner,.layerMaxXMinYCorner]
            
            
            self.hideButtonView()
        }
        // Mark: - IBAction
        @IBAction func DidTapShowButtonView(_ sender: Any) {
            
            UIView.animate(withDuration: 0.5){
                self.showbuttonView()
            }
            
        }
        
        @IBAction func cancelbutton(_ sender: Any) {
            UIView.animate(withDuration: 0.5){
                self.hideButtonView()
            }
        }
        
        
        // Mark: - Private function
        private func showbuttonView(){
            
            if UIScreen.main.bounds.size.height > 800{
                //Bigger device as iPhoneX...
                heightButtonContraint.constant      = 160
                heightStackViewConstraint.constant  = 56
            } else{
                //Small device as Iphone SE...
                heightButtonContraint.constant      = 120
                heightStackViewConstraint.constant  = 35
            }
            
        }
        
        private func hideButtonView(){
            heightButtonContraint.constant      = 0
            heightStackViewConstraint.constant  = 0
        }
        
     
        
    }


