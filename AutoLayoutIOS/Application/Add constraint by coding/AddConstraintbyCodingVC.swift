//
//  ViewController.swift
//  AutoLayoutIOS
//
//  Created by Thorn Loemkimleak on 8/22/22.
//

import UIKit

class AddConstraintbyCodingVC: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var bottomView                   : UIView!
    @IBOutlet weak var heightBottomViewConstraint   : NSLayoutConstraint!
    @IBOutlet weak var heightStackViewConstraint    : NSLayoutConstraint!
    
    var descBottomLabel = UILabel()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set Corner Radius for Bottom View
        self.bottomView.layer.cornerRadius = 16
        self.bottomView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        self.hideBottomView()
        
        //Create constraint by coding
        //Add new label to bottom view
        self.bottomView.addSubview(self.descBottomLabel)
        
        let margins = self.bottomView.layoutMarginsGuide
        self.descBottomLabel.translatesAutoresizingMaskIntoConstraints = false
        self.descBottomLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 20).isActive    = true
        self.descBottomLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -20).isActive = true
        self.descBottomLabel.topAnchor.constraint(equalTo: margins.topAnchor, constant: 20).isActive           = true
        
        //set text to label
        self.descBottomLabel.text = "I am bottom pop up view with animation and stack view inside me ^^"
        self.descBottomLabel.numberOfLines  = 2
        
        
        
    }
    @IBAction func didTapDismissPopUp(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.hideBottomView()
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - IBAction
    @IBAction func didTapShowBottomView(_ sender: Any) {
        UIView.animate(withDuration:0.5) {
            self.showBottomView()
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Private function
    private func showBottomView() {
        self.heightBottomViewConstraint.constant    = 160
        self.heightStackViewConstraint.constant     = 44
        //change constraint dynamic screen2
        if UIScreen.main.bounds.size.height > 800 {
           //Bigger Device (iphone X ...)
            self.heightBottomViewConstraint.constant    = 160
            self.heightStackViewConstraint.constant     = 44
            
        }else {
            //small device (iphone SE...)
            self.heightBottomViewConstraint.constant    = 120
            self.heightStackViewConstraint.constant     = 35
        }
    }
    
    private func hideBottomView() {
        self.heightBottomViewConstraint.constant    = 0
        self.heightStackViewConstraint.constant     = 0
    }

}

